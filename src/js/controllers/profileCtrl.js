
//Profile Controller
angular.module('profileCtrl', [])
.controller('ProfileController', function($scope, $timeout, User){

  User.getInfo().then(function(data){
    $scope.user = data;
  }, function(data){
    console.log("user info retrieval fail");
  });

  User.getPosts().then(function(data){
    $scope.user = data;

    //load slick carousel
    $timeout(function(){
      $(".posts-carousel-ul").slick({
         infinite: true,
         arrows: false,
         slidesToShow: 3,
         slidesToScroll: 3
      });
    }, 10);

  }, function(data){
    console.log("user posts retrieval fail");
  });

  User.getEducation().then(function(data){
    $scope.user = data;
  }, function(data){
    console.log("user education retrieval fail");
  });

  User.getExperience().then(function(data){
    $scope.user = data;
  }, function(data){
    console.log("user experience retrieval fail");
  });

  User.getSkills().then(function(data){
    $scope.user = data;
  }, function(data){
    console.log("user skills retrieval fail");
  });

  User.getRecommendations().then(function(data){
    $scope.user = data;
    $timeout(function(){
      //more reccomendation
      $(".recommendations-list").slick({
         infinite: true,
         arrows: false,
         slidesToShow: 3,
         slidesToScroll: 3
      });
      //more influencer
      $(".influencer-list").slick({
         infinite: true,
         arrows: false,
         slidesToShow: 1,
         slidesToScroll: 1
      });
    }, 70);
  }, function(data){
    console.log("user recommendations retrieval fail");
  });
  //
  User.getFollowing().then(function(data){
    $scope.user = data;
    console.log($scope.user);
  }, function(data){
    console.log("user recommendations retrieval fail");
  });

  User.getAlsoViewed().then(function(data){
    $scope.user = data;
  }, function(data){
    console.log("user also viewed data retrieval fail");
  });

  //functions for slick carousel LEFT and Right
  $scope.carouselLeft = function(){
    $(".posts-carousel-ul").slick('slickPrev');
  };

  $scope.carouselRight = function(){
    $(".posts-carousel-ul").slick('slickNext');
  };

  //functions for slick recommendations carousel LEFT and Right
  $scope.recommendationsLeft = function(){
    $(".recommendations-list").slick('slickPrev');
  };

  $scope.recommendationsRight = function(){
    $(".recommendations-list").slick('slickNext');
  };

  //functions for slick more influencer carousel
  $scope.influencerLeft = function(){
    $(".influencer-list").slick('slickPrev');
  };
  $scope.influencerRight = function(){
    $(".influencer-list").slick('slickNext');
  };

  //following
  $scope.followingMaxItems = 4;
  //visitors
  $scope.visitorMaxItems = 4;

  //random function
  $scope.random = function() {
    return 0.5 - Math.random();
  }
});
