//Users Controller
angular.module('usersCtrl', [])
  .controller("UsersController", function($scope, $http, $location){
    //gets the current location of the path
    $scope.currentPath = $location.path();

    //getting users from linkedin
    $http.get('https://linkedin.ihk.io/profiles/').
      success(function(data) {
          $scope.users = data.data;
    });
  });
