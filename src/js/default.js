// Default JavaScript Functions and Initiations
$(document).ready(function() {

}); // end document ready

//Angular App Variable
var app = angular.module('inventiveApp', ['ngRoute', 'usersCtrl', 'profileCtrl', 'user']);

//Routing
app.config(function($routeProvider, $locationProvider){
  $routeProvider.when("/user/:id",
    {
      templateUrl: "/views/profile.html",
      controller: "ProfileController",
      controllerAs: "profile"
    }
  ).when("/", {
    templateUrl: "/views/users.html",
    controller: "UsersController",
    controllerAs: "users"
  });

  $locationProvider.html5Mode(true);

});

//Constants
app.constant('API', {
  url: 'https://linkedin.ihk.io/'
});

//APP Run
app.run(function($rootScope, $location, API) {

  //API.url to access from the view
  $rootScope.API = API;

  $rootScope.$on("$locationChangeStart", function(event, next, current) {
      // handle route changes
      $rootScope.location = $location.path();
  });
});
