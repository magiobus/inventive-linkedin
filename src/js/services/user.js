//User service

angular.module('user', [])
  .factory('User', function($http, $q, API, $routeParams) {

    var o = {
      info: [],
      posts: [],
      education: [],
      experience: [],
      skills: [],
      recommendations: [],
      following: [],
      alsoViewed:[]
    }

    //get user info
    o.getInfo = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id)
        .success(function(data){
          o.info = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user info");
        });

        return def.promise;
    }

    o.getPosts = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id +'/entries')
        .success(function(data){
          o.posts = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user posts");
        });

        return def.promise;
    }

    o.getEducation = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id +'/education')
        .success(function(data){
          o.education = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user education");
        });

        return def.promise;
    }

    o.getExperience = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id +'/experience')
        .success(function(data){
          o.experience = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user experience");
        });

        return def.promise;
    }

    o.getSkills = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id +'/skills')
        .success(function(data){
          o.skills = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user skills");
        });

        return def.promise;
    }

    o.getRecommendations = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id +'/recommendations')
        .success(function(data){
          o.recommendations = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user skills");
        });

        return def.promise;
    }

    o.getFollowing = function() {
      var def = $q.defer();

      $http.get(API.url + 'profiles' + '/' + $routeParams.id +'/following')
        .success(function(data){
          o.following = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get user skills");
        });

        return def.promise;
    }

    o.getAlsoViewed = function(){
      var def = $q.defer();

      $http.get(API.url + 'profiles/recommendations')
        .success(function(data){
          o.alsoViewed = data.data;
          def.resolve(o);
        })
        .error(function(){
          def.reject("Failed to get also viewed data");
        });

        return def.promise;
    }
    return o;
    });
